package main

import (
	"database/sql"
	"log"
	"net/http"
	"encoding/json"
	"fmt"
	_"github.com/lib/pq"
	"sort"
	"github.com/go-redis/redis"
)

const PORT_NUMBER = 6022
const DB_URL  = "13.229.71.104:26257"
const REDIS_PROVINCE = "listProvince"

type Province struct {
	ProvinceId string `json:"id"`
	ProvinceName string `json:"name"`
	CityName string `json:"cityName"`
}

type City struct {
	CityName []string
}

type ProvinceDetail struct {
	ProvinceId string `json:"id"`
	ProvinceName string `json:"name"`
	CityName []string `json:"cityName"`
}

func conn() *redis.Client {
	client := redis.NewClient(&redis.Options{
		Addr:     "35.194.1.0:6379",
		Password: "t3guhCakep", // no password set
		DB:       7,  // use default DB
	})
	return client
}

func initDb()(db *sql.DB){
	db, err := sql.Open("postgres", "postgresql://root@"+DB_URL+"/gifts?sslmode=disable")
	if err != nil {
		log.Fatal("error connecting : ", err)
	}
	return db
}

func main() {
	db := initDb()
	defer db.Close()
	rdb := conn()
	defer rdb.Close()
	mux := http.NewServeMux()
	mux.HandleFunc("/health", health)
	mux.HandleFunc("/apiv1/province/list", findAll(db, rdb))
	mux.HandleFunc("/apiv1/province/listv2", findAllv2(db, rdb))
	http.ListenAndServe(fmt.Sprintf(":%d", PORT_NUMBER), mux)
}

func health(w http.ResponseWriter, r *http.Request){
	w.WriteHeader(http.StatusOK)
	w.Write([]byte(`{"message":"HEALTH"}`))
}

func findAll(db *sql.DB,rdb *redis.Client) func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		code := http.StatusInternalServerError
		message := `{"message":"Service Not Available"}`

		message,err := rdb.Get(REDIS_PROVINCE).Result()
		if err != nil{
			if err != redis.Nil {
				w.WriteHeader(code)
				w.Write([]byte(err.Error()))
			}else {
				code,message := updateDataService(db,rdb)
				w.WriteHeader(code)
				w.Write([]byte(message))
			}
		}else{
			code,message := updateDataService(db,rdb)
			w.WriteHeader(code)
			w.Write([]byte(message))
		}
		code = http.StatusOK
		w.WriteHeader(code)
		w.Write([]byte(message))
	}
}

func findAllv2(db *sql.DB,rdb *redis.Client) func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		code := http.StatusInternalServerError
		message := `{"message":"Service Not Available"}`

		code,message = updateDataService(db,rdb)

		code = http.StatusOK
		w.WriteHeader(code)
		w.Write([]byte(message))
	}
}

func updateDataService(db *sql.DB,rdb *redis.Client)(int,string) {
	code := http.StatusInternalServerError
	message := `{"message":"Service Not Available"}`
	query := `SELECT p.id_province,p.province_name,c.city_name FROM tb_province p iNNER JOIN tb_city c ON p.id_province = c.id_province; `

	rows, err := db.Query(query)
	if err != nil {
		message = fmt.Sprintf(`{"message":"Error : %s"}`,err.Error())
		return code,message
	}

	defer rows.Close()

	objMaps := map[string]ProvinceDetail{}

	for rows.Next(){
		var obj Province
		var objMap ProvinceDetail

		err = rows.Scan(&obj.ProvinceId,&obj.ProvinceName,&obj.CityName)
		if err != nil {
			message = fmt.Sprintf(`{"message":"Error : %s"}`,err.Error())
			return code,message
		}

		if len(objMaps[obj.ProvinceId].ProvinceName) <= 0 {
			objMap.ProvinceName = obj.ProvinceName
			objMap.CityName = append(objMap.CityName,obj.CityName)
			objMaps[obj.ProvinceId] = objMap
		}else{
			existObjMap := objMaps[obj.ProvinceId].CityName
			objMap.ProvinceName = obj.ProvinceName
			objMap.CityName = append(existObjMap,obj.CityName)
			objMaps[obj.ProvinceId] = objMap
		}
	}

	// Sorting idProvince if needed
	var keys []string
	for key := range objMaps{
		keys = append(keys,key)
	}
	sort.Strings(keys)

	var objs []ProvinceDetail
	for _,key := range keys{
		var obj ProvinceDetail
		obj.ProvinceId = key
		obj.ProvinceName = objMaps[key].ProvinceName
		obj.CityName = objMaps[key].CityName
		objs = append(objs,obj)
	}

	jsonResp,err := json.Marshal(objs)
	if err != nil {
		message = fmt.Sprintf(`{"message":"Error : %s"}`,err.Error())
		return code,message
	}
	/*if err = rdb.Set(REDIS_PROVINCE,jsonResp,-1).Err();err != nil {
		message = fmt.Sprintf(`{"message":"Error : %s"}`,err.Error())
		return code,message
	}*/
	code = http.StatusOK
	message = string(jsonResp)
	return code,message
}